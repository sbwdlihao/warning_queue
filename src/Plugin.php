<?php
/**
 * Created by PhpStorm.
 * User: sbwdlihao
 * Date: 12/29/15
 * Time: 5:19 PM
 */

namespace Numbull\Warning\Queue;

require_once __DIR__.'/../vendor/autoload.php';

use Numbull\Warning\Plugin\PluginImp;
use Requests;

class Plugin extends PluginImp
{
    public function __construct($id = 0)
    {
        parent::__construct($id);
    }

    public function getType()
    {
        return 'table';
    }

    public function getName()
    {
        return 'warning_queue';
    }

    public function getDescription()
    {
        return '监控队列长度和执行异常';
    }

    public function showConfig()
    {
        return [
            'count_url'=>'获取队列长度的url地址',
            'time_url'=>'获取队列最新任务提交的时间的url地址',
            'table'=>'表名',
            'queue_length'=>'队列长度监控,json字符串,比如{"low":1,"middle":2,"high":3}',
            'queue_expire'=>'队列滞留时间监控,json字符串,比如{"low":1,"middle":2,"high":3}',
        ];
    }

    public function action($params)
    {
        if (count($params) != 5) {
            $this->_showResult(201, json_encode($params));
        }
        $count_url = $params[0];
        $time_url = $params[1];
        $table = $params[2];
        $queue_length = json_decode($params[3], true);
        $queue_expire = json_decode($params[4], true);

        $result = [];
        try {
            $response = Requests::post($count_url, [], ['table'=>$table]);
            $body = json_decode($response->body, true);
            if (empty($body)) {
                $this->_showResult(201, sprintf('get %s count response is: %s', $table, $response->body));
            } else {
                $count = (int)$body['data'];
                if ($count == 0 && $body['data'] !== 0) {
                    $result['queue_length'][0]['level'] = 'high';
                    $result['queue_length'][0]['message'] = sprintf('data is not int, data = %s', $body['data']);
                } elseif ($count > $queue_length['high']) {
                    $result['queue_length'][0]['level'] = 'high';
                    $result['queue_length'][0]['message'] = sprintf('%s count is %s over %s', $table, $count, $queue_length['high']);
                } elseif ($count > $queue_length['middle']) {
                    $result['queue_length'][0]['level'] = 'middle';
                    $result['queue_length'][0]['message'] = sprintf('%s count is %s over %s', $table, $count, $queue_length['middle']);
                } elseif ($count > $queue_length['low']) {
                    $result['queue_length'][0]['level'] = 'low';
                    $result['queue_length'][0]['message'] = sprintf('%s count is %s over %s', $table, $count, $queue_length['low']);
                }
            }
        } catch(Exception $e) {
            $this->_showResult(201, $e->getMessage());
        }

        try {
            $response = Requests::post($time_url, [], ['table'=>$table]);
            $body = json_decode($response->body, true);
            if (empty($body)) {
                $this->_showResult(201, sprintf('get %s add time response is: %s', $table, $response->body));
            } else {
                $add_time = (int)$body['data'];
                $now = time();
                $expire = $now - $add_time;
                if ($add_time > 0) {
                    if ($expire > $queue_expire['high']) {
                        $result['queue_expire'][0]['level'] = 'high';
                        $result['queue_expire'][0]['message'] = sprintf('%s last task expire, add time = %s, now = %s, expire time = %s',
                            $table, $add_time, $now, $queue_expire['high']);
                    } elseif ($expire > $queue_expire['middle']) {
                        $result['queue_expire'][0]['level'] = 'middle';
                        $result['queue_expire'][0]['message'] = sprintf('%s last task expire, add time = %s, now = %s, expire time = %s',
                            $table, $add_time, $now, $queue_expire['middle']);
                    } elseif ($expire > $queue_expire['low']) {
                        $result['queue_expire'][0]['level'] = 'low';
                        $result['queue_expire'][0]['message'] = sprintf('%s last task expire, add time = %s, now = %s, expire time = %s',
                            $table, $add_time, $now, $queue_expire['low']);
                    }
                }
            }
        } catch(Exception $e) {
            $this->_showResult(201, $e->getMessage());
        }

        if (!empty($result)) {
            $this->_showResult(301, json_encode($result));
        }

        parent::action($params);
    }
}